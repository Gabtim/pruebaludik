<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    public function index(){
        $usuarios = Usuario::get();
        return compact('usuarios');
    }

    public function records(Request $request)
    {
        return Usuario::SearchRecords($request->acepto)->limit(10)->get();
    }

    public function searchRegister(Request $request){

        $condicion = "";

        if($request->fecha_inicio != null && $request->fecha_fin != null)
        {
            $condicion .= "AND user.fechaRegistro BETWEEN '".$request->fecha_inicio."' AND '".$request->fecha_fin."'";
        }
        if($request->nombre != null)
        {
            $condicion .= " AND user.nombre = $request->nombre";
        }

        $sql = "SELECT user.id, user.Nombre, user.Apellido, user.DNI, user.fechaRegistro
                FROM usuarios user
                WHERE idDisfraz IS NOT NULL $condicion
                ORDER BY fechaRegistro DESC";

        $records = DB::select($sql);

        return $records;
    }

    public function searchRanking(Request $request){
        $condicion = "";

        if($request->idDisfraz != null)
        {
            $condicion .= " AND user.idDisfraz = $request->idDisfraz";
        }

        $sql = "SELECT user.id, user.idDisfraz, user.Nombre, user.Apellido, user.DNI, user.fechaRegistro, part.puntos
                FROM usuarios user
                INNER JOIN partidas part ON part.idJugador = user.id
                WHERE fechaRegistro IS NOT NULL $condicion
                ORDER BY part.puntos DESC";

        $records = DB::select($sql);

        return $records;
    }
}
