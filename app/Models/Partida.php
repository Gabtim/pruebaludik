<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    protected $table = 'partidas';
    public $timestamps = false;

    protected $fillable = [
        'idJugador',
        'idTicket',
        'idTemporada',
        'idcodigo',
        'JuegoToken',
        'estado',
        'puntos',
        'fechaInicio',
        'fechaFin'
    ];

    public function jugador()
    {
        return $this->belongsTo(Usuario::class, 'idJugador');
    }
}
