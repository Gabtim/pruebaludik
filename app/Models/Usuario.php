<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    public $timestamps = false;
    protected $with = ['partidas'];

    protected $fillable = [
        'idDisfraz',
        'Nombre',
        'Apellido',
        'DNI',
        'Celular',
        'Correo',
        'Fecha Nacimiento',
        'Acepto',
        'idProvincia',
        'idDepartamento',
        'fechaRegistro'
    ];

    public function partidas()
    {
        return $this->hasMany(Partida::class, 'idJugador');
    }

    public function scopeSearchRecords($query,$value)
    {
        $concat = 'CONCAT(IF(ISNULL(acepto),"",acepto)," ")';
        $query->selectRaw('DISTINCT usuarios.id, usuarios.*, '.$concat.' as full_description_search');
            
        $query->whereRaw(' '.$concat.' LIKE "%'.$value.'%"');
    }
}
