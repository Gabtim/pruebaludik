<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('usuarios','UsuarioController@index');
Route::get('usuarios/records','UsuarioController@records');
Route::get('usuarios/register','UsuarioController@searchRegister');
Route::get('usuarios/ranking','UsuarioController@searchRanking');
