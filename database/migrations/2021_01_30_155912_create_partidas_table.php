<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidas', function (Blueprint $table) {
            $table->bigIncrements('idJuego');
            $table->bigInteger('idJugador');
            $table->bigInteger('idTicket')->nullable();
            $table->bigInteger('idTemporada');
            $table->bigInteger('idcodigo')->nullable();
            $table->string('JuegoToken')->nullable();
            $table->enum('estado', ['creado', 'inicio', 'finalizado','inactivo'])->nullable();
            $table->float('puntos')->nullable();
            $table->date('fechaInicio')->nullable();
            $table->date('fechaFin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partidas');
    }
}
