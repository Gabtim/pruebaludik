<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idDisfraz');
            $table->text('Nombre')->nullable();
            $table->text('Apellido')->nullable();
            $table->text('DNI')->nullable();
            $table->text('Celular')->nullable();
            $table->text('Correo')->nullable();
            $table->date('FechaNacimiento')->nullable();
            $table->bolean('Acepto')->nullable();
            $table->bigInteger('idProvincia')->nullable();
            $table->bigInteger('idDepartamento')->nullable();
            $table->date('fechaRegistro')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
